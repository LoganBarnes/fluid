import { WglShader, WglTexture } from '@/graphics'

export interface WglProgramData {
  program: WebGLProgram
  successfullyLinked: boolean
}

export class WglProgram {
  private gl: WebGLRenderingContext
  private _data: WglProgramData

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl

    const program: WebGLProgram | null = gl.createProgram()
    if (program === null) {
      throw new Error(`Failed to create WebGL program`)
    }

    this._data = {
      program: program,
      successfullyLinked: false
    }
  }

  get data(): WglProgramData {
    return this._data
  }

  public attachAndLink(...shaders: WglShader[]): WglProgram {
    const gl = this.gl

    for (const shader of shaders) {
      gl.attachShader(this.data.program, shader.data.shader)
    }
    gl.linkProgram(this.data.program)

    if (!gl.getProgramParameter(this.data.program, gl.LINK_STATUS)) {
      throw new Error(`Failed to link program: ${gl.getProgramInfoLog(this.data.program)}`)
    }

    for (const shader of shaders) {
      gl.detachShader(this.data.program, shader.data.shader)
    }

    this._data.successfullyLinked = true
    return this
  }

  public getAttribLocation(name: string): number {
    const gl = this.gl
    const loc: number = gl.getAttribLocation(this.data.program, name)

    if (loc < 0) {
      throw new Error(`Failed to find attribute: ${name}`)
    }

    return loc
  }

  public hasUniform(name: string): boolean {
    const gl = this.gl
    return gl.getUniformLocation(this.data.program, name) !== null
  }

  public getUniformLocation(name: string): WebGLUniformLocation {
    const gl = this.gl
    const loc = gl.getUniformLocation(this.data.program, name)

    if (!loc) {
      throw new Error(`Failed to find uniform: ${name}`)
    }

    return loc
  }

  public use(usage: (boundProgram: InstanceType<typeof WglProgram.Bound>) => void): WglProgram {
    const gl = this.gl
    gl.useProgram(this.data.program)
    usage(new WglProgram.Bound(this))
    gl.useProgram(null)
    return this
  }

  static Bound = class {
    constructor(public self: WglProgram) {
      this.self = self
    }

    get program(): WebGLProgram {
      return this.self._data.program
    }

    public setIntUniform(
      name: string,
      val: Int32List,
      allowFailure: boolean = false
    ): InstanceType<typeof WglProgram.Bound> {
      const gl = this.self.gl

      if (allowFailure && !this.self.hasUniform(name)) {
        return this
      }

      const loc: WebGLUniformLocation = this.self.getUniformLocation(name)

      switch (val.length) {
        case 1:
          gl.uniform1iv(loc, val)
          break
        case 2:
          gl.uniform2iv(loc, val)
          break
        case 3:
          gl.uniform3iv(loc, val)
          break
        case 4:
          gl.uniform4iv(loc, val)
          break
        default:
          throw new Error(`GLSL ivec${val.length} does not exist`)
      }

      return this
    }

    public setFloatUniform(
      name: string,
      val: Float32List,
      allowFailure: boolean = false
    ): InstanceType<typeof WglProgram.Bound> {
      const gl = this.self.gl

      if (allowFailure && !this.self.hasUniform(name)) {
        return this
      }

      const loc: WebGLUniformLocation = this.self.getUniformLocation(name)

      switch (val.length) {
        case 1:
          gl.uniform1fv(loc, val)
          break
        case 2:
          gl.uniform2fv(loc, val)
          break
        case 3:
          gl.uniform3fv(loc, val)
          break
        case 4:
          gl.uniform4fv(loc, val)
          break
        default:
          throw new Error(`GLSL vec${val.length} does not exist`)
      }

      return this
    }

    public setMatrixUniform(
      name: string,
      val: Float32List,
      allowFailure: boolean = false
    ): InstanceType<typeof WglProgram.Bound> {
      const gl = this.self.gl

      if (allowFailure && !this.self.hasUniform(name)) {
        return this
      }

      const loc: WebGLUniformLocation = this.self.getUniformLocation(name)

      switch (val.length) {
        case 4:
          gl.uniformMatrix2fv(loc, false, val)
          break
        case 9:
          gl.uniformMatrix3fv(loc, false, val)
          break
        case 16:
          gl.uniformMatrix4fv(loc, false, val)
          break
        default:
          throw new Error(`GLSL matrix data must contain 4, 9, or 16 elements`)
      }

      return this
    }

    public setTextureUniform(
      name: string,
      texture: InstanceType<typeof WglTexture.Bound>,
      activeTex = 0,
      allowFailure: boolean = false
    ): InstanceType<typeof WglProgram.Bound> {
      const gl = this.self.gl

      if (allowFailure && !this.self.hasUniform(name)) {
        return this
      }

      const loc: WebGLUniformLocation = this.self.getUniformLocation(name)

      texture.activeTexture(activeTex)
      gl.uniform1i(loc, activeTex)
      return this
    }
  }
}
