import { vec2 } from 'gl-matrix'

export interface WglRenderbufferData {
  renderbuffer: WebGLRenderbuffer
  size: vec2
}

export class WglRenderbuffer {
  private gl: WebGLRenderingContext
  private _data: WglRenderbufferData

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl

    const renderbuffer = gl.createRenderbuffer()
    if (renderbuffer === null) {
      throw new Error(`Failed to create Renderbuffer`)
    }

    this._data = {
      renderbuffer,
      size: vec2.create()
    }
  }

  get data(): WglRenderbufferData {
    return this._data
  }

  public bind(usage: (boundTexture: InstanceType<typeof WglRenderbuffer.Bound>) => void): void {
    const gl = this.gl
    gl.bindRenderbuffer(gl.RENDERBUFFER, this.data.renderbuffer)
    usage(new WglRenderbuffer.Bound(this))
    gl.bindRenderbuffer(gl.RENDERBUFFER, null)
  }

  static Bound = class {
    constructor(public self: WglRenderbuffer) {
      this.self = self
    }

    get renderbuffer(): WebGLRenderbuffer {
      return this.self.data.renderbuffer
    }

    public renderbufferStorage(
      internalFormat: number,
      size: vec2
    ): InstanceType<typeof WglRenderbuffer.Bound> {
      const gl = this.self.gl
      this.self.data.size = size
      gl.renderbufferStorage(gl.RENDERBUFFER, internalFormat, size[0], size[1])
      return this
    }
  }
}
