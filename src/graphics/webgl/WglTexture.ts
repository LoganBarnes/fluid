import { vec2, vec3 } from 'gl-matrix'

export interface WglTextureData {
  texture: WebGLTexture
  dimensions: vec3
  format: GLenum
  dataType: GLenum
}

export class WglTexture {
  private gl: WebGLRenderingContext
  private _data: WglTextureData

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl

    const texture = gl.createTexture()
    if (texture === null) {
      throw new Error(`Failed to create Texture`)
    }

    this._data = {
      texture: texture as WebGLTexture,
      dimensions: vec3.create(),
      format: gl.RGBA,
      dataType: gl.UNSIGNED_BYTE
    }
  }

  get data(): WglTextureData {
    return this._data
  }

  public bind(
    type: GLenum,
    usage: (boundTexture: InstanceType<typeof WglTexture.Bound>) => void
  ): void {
    const gl = this.gl
    gl.bindTexture(type, this.data.texture)
    usage(new WglTexture.Bound(this, type))
    gl.bindTexture(type, null)
  }

  static Bound = class {
    constructor(
      public self: WglTexture,
      public readonly type: GLenum
    ) {
      this.self = self
      this.type = type
    }

    get texture(): WebGLTexture {
      return this.self._data.texture
    }

    public setFilterAndWrapParameters(
      filterParam: GLenum,
      wrapParam: GLenum
    ): InstanceType<typeof WglTexture.Bound> {
      const gl = this.self.gl
      this.texParameteri(gl.TEXTURE_MIN_FILTER, filterParam)
      this.texParameteri(gl.TEXTURE_MAG_FILTER, filterParam)
      this.texParameteri(gl.TEXTURE_WRAP_S, wrapParam)
      this.texParameteri(gl.TEXTURE_WRAP_T, wrapParam)
      return this
    }

    public texParameteri(name: GLenum, param: GLenum): InstanceType<typeof WglTexture.Bound> {
      const gl = this.self.gl
      gl.texParameteri(this.type, name, param)
      return this
    }

    public texImage2D(
      size: vec2,
      pixels: ArrayBufferView | null,
      internalFormat: GLenum = this.self.gl.RGBA,
      format: GLenum = this.self.gl.RGBA,
      dataType: GLenum = this.self.gl.UNSIGNED_BYTE,
      level: number = 0,
      border: number = 0
    ): InstanceType<typeof WglTexture.Bound> {
      const gl = this.self.gl

      this.self._data.dimensions = vec3.fromValues(size[0], size[1], 0.0)
      this.self._data.format = format
      this.self._data.dataType = dataType

      gl.texImage2D(
        this.type,
        level,
        internalFormat,
        this.self.data.dimensions[0],
        this.self.data.dimensions[1],
        border,
        this.self.data.format,
        this.self.data.dataType,
        pixels
      )

      return this
    }

    public texSubImage2D(
      pos: vec2,
      size: vec2,
      pixels: ArrayBufferView | null,
      format: GLenum,
      type: GLenum,
      level: number = 0
    ): InstanceType<typeof WglTexture.Bound> {
      const gl = this.self.gl
      gl.texSubImage2D(this.type, level, pos[0], pos[1], size[0], size[1], format, type, pixels)
      return this
    }

    public activeTexture(index: number): InstanceType<typeof WglTexture.Bound> {
      const gl = this.self.gl
      gl.activeTexture(gl.TEXTURE0 + index)
      return this
    }
  }
}
