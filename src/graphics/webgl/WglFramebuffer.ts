import { WglRenderbuffer, WglTexture } from '@/graphics'
import { vec2 } from 'gl-matrix'

export interface WglFramebufferData {
  framebuffer: WebGLFramebuffer
  size: vec2
}

export class WglFramebuffer {
  private gl: WebGLRenderingContext
  private _data: WglFramebufferData

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl

    const framebuffer = gl.createFramebuffer()
    if (framebuffer === null) {
      throw new Error(`Failed to create Framebuffer`)
    }

    this._data = {
      framebuffer,
      size: vec2.create()
    }
  }

  get data(): WglFramebufferData {
    return this._data
  }

  public bind(usage: (boundTexture: InstanceType<typeof WglFramebuffer.Bound>) => void): void {
    const gl = this.gl
    gl.bindFramebuffer(gl.FRAMEBUFFER, this.data.framebuffer)
    usage(new WglFramebuffer.Bound(this))
    gl.bindFramebuffer(gl.FRAMEBUFFER, null)
  }

  static Bound = class {
    constructor(public self: WglFramebuffer) {
      this.self = self
    }

    get framebuffer(): WebGLFramebuffer {
      return this.self._data.framebuffer
    }

    public framebufferRenderbuffer(
      attachment: GLenum,
      renderbuffer: InstanceType<typeof WglRenderbuffer.Bound>
    ): InstanceType<typeof WglFramebuffer.Bound> {
      const gl = this.self.gl
      gl.framebufferRenderbuffer(
        gl.FRAMEBUFFER,
        attachment,
        gl.RENDERBUFFER,
        renderbuffer.renderbuffer
      )
      return this
    }

    public framebufferTexture2D(
      attachment: GLenum,
      texture: InstanceType<typeof WglTexture.Bound>,
      level: number = 0
    ): InstanceType<typeof WglFramebuffer.Bound> {
      const gl = this.self.gl
      if (texture.type !== gl.TEXTURE_2D) {
        throw new Error(`Texture type must be gl.TEXTURE_2D`)
      }
      gl.framebufferTexture2D(gl.FRAMEBUFFER, attachment, texture.type, texture.texture, level)
      return this
    }

    public checkFramebufferStatus(): void {
      const gl = this.self.gl
      const status: GLenum = gl.checkFramebufferStatus(gl.FRAMEBUFFER)

      switch (status) {
        case gl.FRAMEBUFFER_COMPLETE:
          break

        case gl.FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
          throw new Error('Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_ATTACHMENT')

        case gl.FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
          throw new Error('Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT')

        case gl.FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
          throw new Error('Incomplete framebuffer: FRAMEBUFFER_INCOMPLETE_DIMENSIONS')

        case gl.FRAMEBUFFER_UNSUPPORTED:
          throw new Error('Incomplete framebuffer: FRAMEBUFFER_UNSUPPORTED')

        default:
          throw new Error('Incomplete framebuffer: Unknown status')
      }
    }
  }
}
