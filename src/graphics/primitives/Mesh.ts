import type { vec3, vec2 } from 'gl-matrix'

export enum MeshType {
  POINTS = 0,
  LINES = 1,
  LINE_STRIP = 2,
  TRIANGLES = 3,
  TRIANGLE_STRIP = 4,
  TRIANGLE_FAN = 5
}

// Unit cube centered on origin
export class Mesh3 {
  public positions: vec3[] = []
  public normals: vec3[] = []
  public uvs: vec2[] = []

  public indices: number[] = []

  public type: MeshType

  constructor(type: MeshType) {
    this.type = type
  }

  public getInterleavedVertexData(): Float32Array {
    if (this.positions.length !== this.normals.length) {
      throw new Error(`positions and normals must be the same length`)
    }
    if (this.positions.length !== this.uvs.length) {
      throw new Error(`positions and uvs must be the same length`)
    }

    const data: Float32Array = new Float32Array(this.positions.length * 8)

    for (let i = 0; i < this.positions.length; i++) {
      data[i * 8 + 0] = this.positions[i][0]
      data[i * 8 + 1] = this.positions[i][1]
      data[i * 8 + 2] = this.positions[i][2]
      data[i * 8 + 3] = this.normals[i][0]
      data[i * 8 + 4] = this.normals[i][1]
      data[i * 8 + 5] = this.normals[i][2]
      data[i * 8 + 6] = this.uvs[i][0]
      data[i * 8 + 7] = this.uvs[i][1]
    }

    return data
  }

  public getIndexData(): Uint16Array {
    return new Uint16Array(this.indices)
  }
}
