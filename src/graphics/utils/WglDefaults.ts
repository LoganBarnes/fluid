export function setWglDefaults(gl: WebGLRenderingContext) {
  gl.clearColor(0.0, 0.0, 0.0, 1.0)

  // Enable depth testing, so that objects are occluded based
  // on depth instead of drawing order.
  gl.enable(gl.DEPTH_TEST)

  // Move the polygons back a bit so lines are still drawn
  // even though they are coplanar with the polygons they
  // came from, which will be drawn before them.
  gl.enable(gl.POLYGON_OFFSET_FILL)
  gl.polygonOffset(-1, -1)

  // Enable back-face culling, meaning only the front side
  // of every face is rendered.
  gl.enable(gl.CULL_FACE)
  gl.cullFace(gl.BACK)

  // Specify that the front face is represented by vertices in
  // a counterclockwise order (this is the default).
  gl.frontFace(gl.CCW)
}
