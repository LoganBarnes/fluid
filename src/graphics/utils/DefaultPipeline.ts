import { Mesh3, MeshType, WglBuffer, WglProgram, WglShader, WglVertexArray } from '@/graphics'
import defaultVert from '@/assets/shaders/default.vert'
import defaultFrag from '@/assets/shaders/default.frag'
import { vec3, mat3, mat4 } from 'gl-matrix'

interface PipelineItem {
  vbo: WglBuffer
  ibo?: WglBuffer
  mode: GLenum
  drawCount: number
  color: vec3
}

function primitiveType(gl: WebGLRenderingContext, meshType: MeshType): GLenum {
  switch (meshType) {
    case MeshType.POINTS:
      return gl.POINTS
    case MeshType.LINES:
      return gl.LINES
    case MeshType.LINE_STRIP:
      return gl.LINE_STRIP
    case MeshType.TRIANGLES:
      return gl.TRIANGLES
    case MeshType.TRIANGLE_STRIP:
      return gl.TRIANGLE_STRIP
    case MeshType.TRIANGLE_FAN:
      return gl.TRIANGLE_FAN
    default:
      throw new Error(`unknown primitive type ${meshType}`)
  }
}

function makeItem(gl: WebGLRenderingContext, mesh: Mesh3, color: vec3): PipelineItem {
  const vbo = new WglBuffer(gl).bind(gl.ARRAY_BUFFER, (boundVbo) => {
    boundVbo.bufferData(mesh.getInterleavedVertexData())
  })

  if (mesh.indices.length > 0) {
    return {
      vbo: vbo,
      ibo: new WglBuffer(gl).bind(gl.ELEMENT_ARRAY_BUFFER, (boundIbo) => {
        boundIbo.bufferData(mesh.getIndexData())
      }),
      mode: primitiveType(gl, mesh.type),
      drawCount: mesh.indices.length,
      color: color
    }
  } else {
    return {
      vbo: vbo,
      mode: primitiveType(gl, mesh.type),
      drawCount: mesh.positions.length,
      color: color
    }
  }
}

export class DefaultPipeline {
  private gl: WebGLRenderingContext
  private program: WglProgram
  private vao: WglVertexArray

  private identity3 = mat3.create()
  private identity4 = mat4.create()

  private items: PipelineItem[] = []

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl
    this.program = new WglProgram(gl).attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(defaultVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(defaultFrag)
    )

    this.vao = new WglVertexArray(gl, (3 + 3 + 2) * 4, [
      {
        location: this.program.getAttribLocation(`localPosition`),
        size: 3,
        type: gl.FLOAT,
        offset: 0
      },
      {
        location: this.program.getAttribLocation(`localNormal`),
        size: 3,
        type: gl.FLOAT,
        offset: 3 * 4
      },
      {
        location: this.program.getAttribLocation(`uv`),
        size: 2,
        type: gl.FLOAT,
        offset: (3 + 3) * 4
      }
    ])
  }

  public clear(): DefaultPipeline {
    this.items = []
    return this
  }

  public addMesh(mesh: Mesh3, color: vec3): number {
    const gl = this.gl

    const index = this.items.length
    this.items.push(makeItem(gl, mesh, color))

    return index
  }

  public updateMesh(index: number, mesh: Mesh3, color: vec3): DefaultPipeline {
    const gl = this.gl

    this.items[index] = makeItem(gl, mesh, color)

    return this
  }

  public draw(): DefaultPipeline {
    const gl = this.gl

    this.program.use((boundProgram) => {
      boundProgram
        .setMatrixUniform(`screenFromWorld`, this.identity4)
        .setMatrixUniform(`worldFromLocal`, this.identity4)
        .setMatrixUniform(`worldFromLocalNormal`, this.identity3)

      this.items.forEach((item) => {
        item.vbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
          this.vao.enableAttribs(boundVbo)

          boundProgram.setIntUniform(`displayMode`, [4]).setFloatUniform(`uniformColor`, item.color)

          if (item.ibo) {
            item.ibo.bind(gl.ELEMENT_ARRAY_BUFFER, (boundIbo) => {
              this.vao.drawElements(
                boundProgram,
                boundVbo,
                boundIbo,
                item.mode,
                item.drawCount,
                gl.UNSIGNED_SHORT,
                0
              )
            })
          } else {
            this.vao.drawArrays(boundProgram, boundVbo, item.mode, 0, item.drawCount)
          }
        })
      })
    })

    return this
  }
}
