import { WglBuffer, WglProgram, WglShader, WglVertexArray } from '@/graphics'
import screenspaceVert from '@/assets/shaders/screenspace.vert'

export class ScreenspacePipeline {
  private gl: WebGLRenderingContext
  private program: WglProgram
  private vao: WglVertexArray
  private vbo: WglBuffer

  constructor(gl: WebGLRenderingContext, frag: string, pipeline?: ScreenspacePipeline) {
    this.gl = gl
    this.program = new WglProgram(gl).attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(screenspaceVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(frag)
    )
    this.vao = new WglVertexArray(gl, 0, [
      {
        location: this.program.getAttribLocation(`clipPosition`),
        size: 2,
        type: gl.FLOAT,
        offset: 0
      }
    ])
    if (pipeline) {
      this.vbo = pipeline.vbo
    } else {
      this.vbo = new WglBuffer(gl).bind(gl.ARRAY_BUFFER, (boundBuffer) => {
        boundBuffer.bufferData(new Float32Array([-1, -1, +1, -1, -1, +1, +1, +1]))
      })
    }
  }

  public bind(
    usage: (boundPipeline: InstanceType<typeof ScreenspacePipeline.Bound>) => void
  ): void {
    const gl = this.gl

    this.vbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
      this.vao.enableAttribs(boundVbo)

      this.program.use((boundProgram) => {
        usage(new ScreenspacePipeline.Bound(this, boundProgram, boundVbo))
      })
    })
  }

  static Bound = class {
    constructor(
      public self: ScreenspacePipeline,
      public boundProgram: InstanceType<typeof WglProgram.Bound>,
      public boundVbo: InstanceType<typeof WglBuffer.Bound>
    ) {
      this.self = self
      this.boundProgram = boundProgram
      this.boundVbo = boundVbo
    }

    public draw(): InstanceType<typeof ScreenspacePipeline.Bound> {
      const gl = this.self.gl

      this.self.vao.drawArrays(this.boundProgram, this.boundVbo, gl.TRIANGLE_STRIP, 0, 4)

      return this
    }
  }
}
