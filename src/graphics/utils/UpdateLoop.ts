// A lot of these should not be floats but this is web land so we don't really have a choice.
export interface UpdateLoopStatus {
  pauseUpdates: boolean
  exitUpdateLoop: boolean

  timeScale: number
  cumulativeTimeSecs: number
  updateTimeStepSecs: number
  minimumUpdateTimeStepSecs: number
  interpolantBetweenUpdates: number // 0.0 is previous update, 1.0 is the next update.
}

export class UpdateLoop {
  // The accumulator maintains the relationship between cpu time and
  // simulation time. It is manipulated in two ways:
  //
  // - On each frame the accumulator increases by the time it took
  //   to run the previous frame (cpu time).
  // - After each simulation update the accumulator is decremented
  //   by the constant simulation time step (sim time).
  //
  // This loop will continuously update the simulation until the
  // simulation's time has caught up to the expected cpu "real time".
  // This is done by comparing the accumulator's value to the expected
  // simulation time step. If the accumulator is less than the time step
  // interval, the loop will be allowed to continue on to rendering.
  private accumulatorMs: number
  private status: UpdateLoopStatus
  private timeOfLastUpdate: number

  private fixedStepUpdate: (status: UpdateLoopStatus) => void
  private frameUpdate: (status: UpdateLoopStatus) => void

  constructor(
    onFixedStepUpdate: (status: UpdateLoopStatus) => void,
    onFrameUpdate: (status: UpdateLoopStatus) => void,
    onInit?: (status: UpdateLoopStatus) => void
  ) {
    this.fixedStepUpdate = onFixedStepUpdate
    this.frameUpdate = onFrameUpdate

    this.accumulatorMs = 0.0
    this.status = {
      pauseUpdates: false,
      exitUpdateLoop: false,
      timeScale: 1.0,
      cumulativeTimeSecs: 0.0,
      updateTimeStepSecs: 1.0 / 60.0,
      minimumUpdateTimeStepSecs: 0.1,
      interpolantBetweenUpdates: 0.0
    }

    if (onInit !== undefined) {
      onInit(this.status)
    }

    this.timeOfLastUpdate = Date.now()
  }

  public run(): void {
    const newTime = Date.now()
    let frameTimeMs: number = newTime.valueOf() - this.timeOfLastUpdate.valueOf()

    this.timeOfLastUpdate = newTime
    frameTimeMs = Math.min(this.status.minimumUpdateTimeStepSecs * 1000.0, frameTimeMs)
    frameTimeMs = frameTimeMs * this.status.timeScale

    // Ignore updates when paused.
    if (!this.status.pauseUpdates) {
      this.accumulatorMs += frameTimeMs

      let updateTimeStepMs: number = this.status.updateTimeStepSecs * 1000.0

      // Continually update the simulation until the sim
      // time is within one time step of the cpu time.
      while (this.accumulatorMs >= updateTimeStepMs) {
        this.status.cumulativeTimeSecs += updateTimeStepMs
        this.accumulatorMs -= updateTimeStepMs
        this.fixedStepUpdate(this.status)

        updateTimeStepMs = this.status.updateTimeStepSecs * 1000.0
      }
      // `interpolantBetweenUpdates` is the normalized (0,1] interpolation value
      // between the last update and the current update when this render call is made
      // (optionally used to interpolate between previous and current states).
      this.status.interpolantBetweenUpdates = this.accumulatorMs / updateTimeStepMs

      // The value is clamped in case the `update_time_step` is
      // modified by the user inside `fixedStepUpdate()`.
      this.status.interpolantBetweenUpdates = Math.min(
        Math.max(this.status.interpolantBetweenUpdates, 0.0),
        1.0
      )
    }

    this.frameUpdate(this.status)

    if (!this.status.exitUpdateLoop) {
      window.requestAnimationFrame(this.run.bind(this))
    }
  }

  public stop(): void {
    this.status.exitUpdateLoop = true
  }
}
