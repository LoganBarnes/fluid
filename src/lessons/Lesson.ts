import { setWglDefaults } from '@/graphics'
import type { UpdateLoopStatus } from '@/graphics'
import { vec2 } from 'gl-matrix'

export class Lesson {
  protected gl: WebGLRenderingContext
  protected size: vec2

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl
    setWglDefaults(gl)
    this.size = vec2.fromValues(1, 1)
  }

  public resize(width: number, height: number) {
    this.size = vec2.fromValues(width, height)
  }

  public getTimeStepSecs(): number {
    return 0
  }

  public init(_status: UpdateLoopStatus) {}

  public fixedStepUpdate(_status: UpdateLoopStatus) {}

  public frameUpdate(_status: UpdateLoopStatus) {}

  public onMouse(_event: MouseEvent): void {}

  public onKey(_event: KeyboardEvent): void {}

  public onWheel(_event: WheelEvent): void {}
}
