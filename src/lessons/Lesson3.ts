import { Lesson } from '@/lessons/Lesson'
import { DefaultPipeline, Mesh3, MeshType } from '@/graphics'
import type { UpdateLoopStatus } from '@/graphics'
import { FlowPipeline } from '@/lessons/Flow'
import type { FlowOptions } from '@/lessons/Flow'
import lesson3Frag from '@/assets/shaders/lesson3.frag'

export interface Lesson3DisplayOptions {
  displayDomain: boolean
  displayGrid: boolean
}

export class Lesson3 extends Lesson {
  private defaultPipeline: DefaultPipeline
  private flowPipeline: FlowPipeline

  private flowOptions: FlowOptions
  private displayOptions: Lesson3DisplayOptions

  private timeStepSecs: number = 0

  constructor(
    gl: WebGLRenderingContext,
    flowOptions: FlowOptions,
    displayOptions: Lesson3DisplayOptions
  ) {
    super(gl)

    // Disable depth testing, so that objects are occluded based on drawing order.
    gl.disable(gl.DEPTH_TEST)

    this.defaultPipeline = new DefaultPipeline(gl)
    this.flowPipeline = new FlowPipeline(gl, lesson3Frag)

    this.flowOptions = flowOptions
    this.displayOptions = displayOptions
    this.setFlowOptions(this.flowOptions)
  }

  public setFlowOptions(options: Partial<FlowOptions>) {
    this.flowOptions = { ...this.flowOptions, ...options }

    const sigma = 0.2
    const dx = this.flowOptions.gridSize
    const nu = this.flowOptions.viscosity
    this.timeStepSecs = (sigma * dx * dx) / nu

    this.flowPipeline.setOptions(this.flowOptions)

    this.setDisplayOptions(this.displayOptions)
  }

  public setDisplayOptions(options: Partial<Lesson3DisplayOptions>) {
    this.displayOptions = { ...this.displayOptions, ...options }

    this.defaultPipeline.clear()

    if (this.displayOptions.displayGrid) {
      const gridAxis = new Mesh3(MeshType.LINES)

      const gridLines = this.flowOptions.domainSize / this.flowOptions.gridSize

      for (let i = 1; i < gridLines; i++) {
        const t = i / gridLines
        const x = t - 1 + t
        gridAxis.positions.push([x, -1, 0], [x, 1, 0])
      }
      gridAxis.normals = Array(gridAxis.positions.length).fill([0, 0, 0])
      gridAxis.uvs = Array(gridAxis.positions.length).fill([0, 0, 0])

      this.defaultPipeline.addMesh(gridAxis, [0.2, 0.2, 0.2])
    }

    if (this.displayOptions.displayDomain) {
      const domainAxis = new Mesh3(MeshType.LINES)
      domainAxis.positions.push([-1, 0, 0], [1, 0, 0])
      domainAxis.positions.push([-1, -0.1, 0], [-1, 0.1, 0])

      for (let i = 0; i < this.flowOptions.domainSize; i++) {
        const t = (i + 1) / this.flowOptions.domainSize
        const x = t - 1 + t
        domainAxis.positions.push([x, -0.1, 0], [x, 0.1, 0])
      }
      domainAxis.normals = Array(domainAxis.positions.length).fill([0, 0, 0])
      domainAxis.uvs = Array(domainAxis.positions.length).fill([0, 0, 0])

      this.defaultPipeline.addMesh(domainAxis, [0, 1, 0])
    }
  }

  public resize(width: number, height: number) {
    super.resize(width, height)
  }

  public getTimeStepSecs(): number {
    return this.timeStepSecs
  }

  public fixedStepUpdate(status: UpdateLoopStatus) {
    if (this.timeStepSecs !== status.updateTimeStepSecs) {
      status.updateTimeStepSecs = this.timeStepSecs
    }
    this.flowPipeline.update(status)
  }

  public frameUpdate(_status: UpdateLoopStatus) {
    const gl = this.gl

    gl.viewport(0, 0, this.size[0], this.size[1])
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    this.defaultPipeline.draw()
    this.flowPipeline.draw()
  }
}
