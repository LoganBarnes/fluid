import {
  setWglDefaults,
  ScreenspacePipeline,
  UpdateLoop,
  WglFramebuffer,
  WglRenderbuffer,
  WglTexture
} from '@/graphics'
import type { UpdateLoopStatus } from '@/graphics'
import initialFlowFrag from '@/assets/shaders/initialFlow.frag'
import { vec2 } from 'gl-matrix'

const rho0 = 100 // average density
const tau = 0.6 // collision timescale

class Flow {
  public texture: WglTexture
  public renderbuffer: WglRenderbuffer
  public framebuffer: WglFramebuffer

  constructor(gl: WebGLRenderingContext) {
    this.texture = new WglTexture(gl)
    this.renderbuffer = new WglRenderbuffer(gl)
    this.framebuffer = new WglFramebuffer(gl)
  }

  public rebind(gl: WebGLRenderingContext): Flow {
    this.framebuffer.bind((boundFramebuffer) => {
      this.renderbuffer.bind((boundRenderbuffer) => {
        boundFramebuffer.framebufferRenderbuffer(gl.DEPTH_ATTACHMENT, boundRenderbuffer)
      })

      this.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundFramebuffer.framebufferTexture2D(gl.COLOR_ATTACHMENT0, boundTexture)
      })

      boundFramebuffer.checkFramebufferStatus()
    })

    return this
  }
}

export class LessonTest {
  private gl: WebGLRenderingContext
  private updateLoop: UpdateLoop

  private flowPipeline: ScreenspacePipeline
  private displayPipeline: ScreenspacePipeline

  private size: vec2

  private flow: Flow[]
  private frame: number

  constructor(gl: WebGLRenderingContext) {
    this.gl = gl
    this.updateLoop = new UpdateLoop(this.fixedStepUpdate.bind(this), this.frameUpdate.bind(this))

    setWglDefaults(gl)

    this.flowPipeline = new ScreenspacePipeline(gl, initialFlowFrag)
    this.displayPipeline = new ScreenspacePipeline(gl, initialFlowFrag, this.flowPipeline)

    this.size = vec2.fromValues(1, 1)
    this.flow = [new Flow(gl), new Flow(gl)]
    this.frame = 0
  }

  public run(): void {
    this.updateLoop.run()
  }

  public stop(): void {
    this.updateLoop.stop()
  }

  public resize(width: number, height: number) {
    const gl = this.gl

    this.size = vec2.fromValues(width, height)

    this.flow.forEach((flow) => {
      flow.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundTexture.setFilterAndWrapParameters(gl.NEAREST, gl.CLAMP_TO_EDGE)
        boundTexture.texImage2D(this.size, null, gl.RGBA, gl.RGBA, gl.FLOAT)
      })

      flow.renderbuffer.bind((renderbuffer) => {
        renderbuffer.renderbufferStorage(gl.DEPTH_COMPONENT16, this.size)
      })

      flow.rebind(gl)
    })

    this.frame = 0
    const flow = this.flow[this.frame]

    flow.framebuffer.bind((_) => {
      this.gl.viewport(0, 0, this.size[0], this.size[1])
      this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)
    })
  }

  private fixedStepUpdate(_status: UpdateLoopStatus) {
    const flowCurr = this.flow[this.frame]
    const flowPrev = this.flow[(this.frame + 1) % 2]

    flowCurr.framebuffer.bind((_) => {
      this.gl.viewport(0, 0, this.size[0], this.size[1])
      this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)

      this.displayPipeline.bind((boundPipeline) => {
        flowPrev.texture.bind(this.gl.TEXTURE_2D, (boundTexture) => {
          boundTexture.activeTexture(0)

          boundPipeline.boundProgram.setTextureUniform(`flowMap`, boundTexture, 0)

          boundPipeline.draw()
        })
      })
    })

    this.frame = (this.frame + 1) % 2
  }

  private frameUpdate(_status: UpdateLoopStatus) {
    const flow = this.flow[this.frame]

    this.gl.viewport(0, 0, this.size[0], this.size[1])
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)

    this.displayPipeline.bind((boundPipeline) => {
      flow.texture.bind(this.gl.TEXTURE_2D, (boundTexture) => {
        boundTexture.activeTexture(0)

        boundPipeline.boundProgram.setTextureUniform(`flowMap`, boundTexture, 0)

        boundPipeline.draw()
      })
    })
  }
}
