import { Lesson } from '@/lessons/Lesson'
import {
  setWglDefaults,
  Cube,
  UpdateLoop,
  WglBuffer,
  WglProgram,
  WglShader,
  WglVertexArray
} from '@/graphics'
import type { UpdateLoopStatus } from '@/graphics'
import defaultVert from '@/assets/shaders/default.vert'
import defaultFrag from '@/assets/shaders/default.frag'
import { glMatrix, vec2, vec3, mat3, mat4, quat } from 'gl-matrix'

export class LessonCube extends Lesson {
  private program: WglProgram
  private vbo: WglBuffer
  private ibo: WglBuffer
  private vao: WglVertexArray

  private mode: GLenum
  private drawCount: number

  private cubeRotation: quat[]
  private cubeTranslation: vec3[]

  constructor(gl: WebGLRenderingContext) {
    super(gl)

    this.program = new WglProgram(gl)

    this.program.attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(defaultVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(defaultFrag)
    )

    {
      const cube = new Cube()

      this.vbo = new WglBuffer(gl)
      this.vbo.bind(gl.ARRAY_BUFFER, (boundBuffer) => {
        boundBuffer.bufferData(cube.buildArrayWithAllVertexData())
      })

      this.ibo = new WglBuffer(gl)
      this.ibo.bind(gl.ELEMENT_ARRAY_BUFFER, (boundBuffer) => {
        boundBuffer.bufferData(cube.indexData)
      })

      this.vao = new WglVertexArray(gl, 0, [
        {
          location: this.program.getAttribLocation(`localPosition`),
          size: 3,
          type: gl.FLOAT,
          offset: 0
        },
        {
          location: this.program.getAttribLocation(`localNormal`),
          size: 3,
          type: gl.FLOAT,
          offset: cube.getVertexDataSizeInBytes()
        },
        {
          location: this.program.getAttribLocation(`uv`),
          size: 2,
          type: gl.FLOAT,
          offset: cube.getVertexDataSizeInBytes() + cube.getNormalDataSizeInBytes()
        }
      ])

      this.mode = cube.getPrimitiveType(gl)
      this.drawCount = cube.indexData.length
    }

    this.cubeRotation = [quat.create(), quat.create()]
    this.cubeTranslation = [vec3.create(), vec3.create()]
  }

  public fixedStepUpdate(status: UpdateLoopStatus) {
    quat.copy(this.cubeRotation[0], this.cubeRotation[1])
    quat.setAxisAngle(
      this.cubeRotation[1],
      vec3.normalize(vec3.create(), vec3.fromValues(1, 3, 2)),
      status.cumulativeTimeSecs * 0.001
    )
  }

  public frameUpdate(status: UpdateLoopStatus) {
    this.gl.viewport(0, 0, this.size[0], this.size[1])
    this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT)

    const cubeRotation = quat.lerp(
      quat.create(),
      this.cubeRotation[0],
      this.cubeRotation[1],
      status.interpolantBetweenUpdates
    )
    const cubeTranslation = vec3.lerp(
      vec3.create(),
      this.cubeTranslation[0],
      this.cubeTranslation[1],
      status.interpolantBetweenUpdates
    )

    const cubeWorldFromLocal = mat4.fromRotationTranslation(
      mat4.create(),
      cubeRotation,
      cubeTranslation
    )
    const cubeWorldFromLocalNormals = mat3.normalFromMat4(mat3.create(), cubeWorldFromLocal)

    const clipFromWorld = mat4.lookAt(
      mat4.create(),
      vec3.fromValues(2, 1, 10),
      vec3.fromValues(0, 0, 0),
      vec3.fromValues(0, 1, 0)
    )

    const screenFromClip = mat4.perspective(
      mat4.create(),
      glMatrix.toRadian(45),
      this.size[0] / this.size[1],
      0.1,
      100
    )

    const screenFromWorld = mat4.mul(mat4.create(), screenFromClip, clipFromWorld)

    this.vbo.bind(this.gl.ARRAY_BUFFER, (boundVbo) => {
      this.vao.enableAttribs(boundVbo)

      this.ibo.bind(this.gl.ELEMENT_ARRAY_BUFFER, (boundIbo) => {
        this.program.use((boundProgram) => {
          boundProgram
            .setMatrixUniform(`screenFromWorld`, screenFromWorld)
            .setMatrixUniform(`worldFromLocal`, cubeWorldFromLocal)
            .setMatrixUniform(`worldFromLocalNormal`, cubeWorldFromLocalNormals)
            .setIntUniform(`displayMode`, [1])

          this.vao.drawElements(
            boundProgram,
            boundVbo,
            boundIbo,
            this.mode,
            this.drawCount,
            this.gl.UNSIGNED_SHORT,
            0
          )
        })
      })
    })
  }
}
