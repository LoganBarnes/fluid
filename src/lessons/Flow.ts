import {
  ScreenspacePipeline,
  WglBuffer,
  WglFramebuffer,
  WglProgram,
  WglRenderbuffer,
  WglShader,
  WglTexture,
  WglVertexArray
} from '@/graphics'
import type { UpdateLoopStatus } from '@/graphics'
import flowDisplayVert from '@/assets/shaders/flowDisplay.vert'
import flowDisplayFrag from '@/assets/shaders/flowDisplay.frag'
import { vec2 } from 'gl-matrix'

export interface FlowOptions {
  domainSize: number
  gridSize: number
  waveSpeed: number
  nonLinear: boolean
  viscosity: number
}

class Flow {
  public texture: WglTexture
  public renderbuffer: WglRenderbuffer
  public framebuffer: WglFramebuffer

  constructor(gl: WebGLRenderingContext) {
    this.texture = new WglTexture(gl)
    this.renderbuffer = new WglRenderbuffer(gl)
    this.framebuffer = new WglFramebuffer(gl)
  }

  public rebind(gl: WebGLRenderingContext): Flow {
    this.framebuffer.bind((boundFramebuffer) => {
      this.renderbuffer.bind((boundRenderbuffer) => {
        boundFramebuffer.framebufferRenderbuffer(gl.DEPTH_ATTACHMENT, boundRenderbuffer)
      })

      this.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundFramebuffer.framebufferTexture2D(gl.COLOR_ATTACHMENT0, boundTexture)
      })

      boundFramebuffer.checkFramebufferStatus()
    })

    return this
  }
}

function getNextFrame(frame: number): number {
  return (frame + 1) % 2
}

export class FlowPipeline {
  private gl: WebGLRenderingContext

  private frame: number
  private flows: Flow[]
  private flowPipeline: ScreenspacePipeline

  private displayProgram: WglProgram
  private vao: WglVertexArray
  private vbo: WglBuffer

  private size: vec2 = [1, 1]
  private gridSize: number = 1
  private waveSpeed: number = 1.0
  private nonLinearFactor: number = 0.0
  private viscosity: number = 1.0

  private scaleFactor: number

  constructor(gl: WebGLRenderingContext, lessonFrag: string, scaleFactor: number = 0.4) {
    this.gl = gl

    this.scaleFactor = scaleFactor

    this.frame = 0
    this.flows = [new Flow(gl), new Flow(gl)]
    this.flowPipeline = new ScreenspacePipeline(gl, lessonFrag)

    this.displayProgram = new WglProgram(gl).attachAndLink(
      new WglShader(gl, gl.VERTEX_SHADER).loadAndCompile(flowDisplayVert),
      new WglShader(gl, gl.FRAGMENT_SHADER).loadAndCompile(flowDisplayFrag)
    )

    this.vao = new WglVertexArray(gl, 0, [
      {
        location: this.displayProgram.getAttribLocation(`index`),
        size: 3,
        type: gl.FLOAT,
        offset: 0
      }
    ])

    this.vbo = new WglBuffer(gl).bind(gl.ARRAY_BUFFER, (boundBuffer) => {
      boundBuffer.bufferData(new Float32Array([0.5, 0.5, 0]))
    })
  }

  public setOptions(options: FlowOptions, initialization?: (x: number) => number) {
    const gl = this.gl

    this.size = vec2.fromValues(Math.floor(options.domainSize / options.gridSize), 1)
    this.gridSize = options.gridSize
    this.waveSpeed = options.waveSpeed
    this.nonLinearFactor = options.nonLinear ? 1.0 : 0.0
    this.viscosity = options.viscosity

    this.flows.forEach((flow) => {
      flow.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        boundTexture.setFilterAndWrapParameters(gl.NEAREST, gl.CLAMP_TO_EDGE)
        boundTexture.texImage2D(this.size, null, gl.RGBA, gl.RGBA, gl.FLOAT)
      })

      flow.renderbuffer.bind((renderbuffer) => {
        renderbuffer.renderbufferStorage(gl.DEPTH_COMPONENT16, this.size)
      })

      flow.rebind(gl)
    })

    this.frame = 0
    const flow = this.flows[this.frame]

    flow.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
      boundTexture.texSubImage2D(
        [0, 0],
        [this.size[0], 1],
        new Float32Array(this.size[0] * 4).map((_, i) => {
          const cell = Math.floor(i / 4)
          const x = cell * options.gridSize

          if (initialization) {
            return initialization(x)
          } else if (i % 4 === 0 && x > 0.5 && x < 1.0) {
            return 2
          } else {
            return 1
          }
        }),
        gl.RGBA,
        gl.FLOAT
      )
    })

    this.vbo.bind(gl.ARRAY_BUFFER, (boundBuffer) => {
      boundBuffer.bufferData(
        new Float32Array(this.size[0] * 6).map((_, i) => {
          switch (i % 6) {
            case 0:
            case 3:
              return Math.floor(i / 6) + 0.5
            case 1:
            case 4:
              return 0.5
            case 5:
              return 1
            case 2:
              break
          }
          return 0
        })
      )
    })
  }

  public update(status: UpdateLoopStatus) {
    const gl = this.gl

    const currFrame = this.frame
    const nextFrame = getNextFrame(currFrame)

    const currFlow = this.flows[currFrame]
    const nextFlow = this.flows[nextFrame]

    nextFlow.framebuffer.bind((_) => {
      gl.viewport(0, 0, this.size[0], this.size[1])
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

      this.flowPipeline.bind((boundPipeline) => {
        currFlow.texture.bind(gl.TEXTURE_2D, (boundTexture) => {
          boundPipeline.boundProgram
            .setFloatUniform(`nonLinear`, [this.nonLinearFactor], true)
            .setFloatUniform(`c`, [this.waveSpeed], true)
            .setFloatUniform(`dx`, [this.gridSize], true)
            .setFloatUniform(`dt`, [status.updateTimeStepSecs], true)
            .setFloatUniform(`nu`, [this.viscosity], true)
            .setFloatUniform(`flowSize`, this.size, true)
            .setTextureUniform(`flowMap`, boundTexture, 0)
          boundPipeline.draw()
        })
      })
    })

    this.frame = nextFrame
  }

  public draw() {
    const gl = this.gl

    this.vbo.bind(gl.ARRAY_BUFFER, (boundVbo) => {
      this.vao.enableAttribs(boundVbo)

      this.flows[this.frame].texture.bind(gl.TEXTURE_2D, (boundTexture) => {
        this.displayProgram.use((boundProgram) => {
          boundProgram
            .setFloatUniform(`flowSize`, this.size)
            .setTextureUniform(`flowMap`, boundTexture, 0)
            .setFloatUniform(`scaleFactor`, [this.scaleFactor])

          this.vao.drawArrays(boundProgram, boundVbo, gl.LINES, 0, this.size[0] * 2)
        })
      })
    })
  }
}
