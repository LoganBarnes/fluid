import { Lesson } from '@/lessons/Lesson'
import { DefaultPipeline, Mesh3, MeshType } from '@/graphics'
import type { UpdateLoopStatus } from '@/graphics'
import { FlowPipeline } from '@/lessons/Flow'
import type { FlowOptions } from '@/lessons/Flow'
import lesson4Frag from '@/assets/shaders/lesson4.frag'

function uFunc(t: number, x: number, nu: number): number {
  const leftNumer = x - 4 * t
  const rightNumer = x - 4 * t - 2 * Math.PI
  const denom = 4 * nu * (t + 1)

  const leftExp = Math.exp(-(leftNumer * leftNumer) / denom)
  const rightExp = Math.exp(-(rightNumer * rightNumer) / denom)

  const phi = leftExp + rightExp

  const leftPrime = (-leftExp / denom) * (2 * x - 8 * t)
  const rightPrime = (-rightExp / denom) * (2 * x - 8 * t - 4 * Math.PI)

  const phiPrime = leftPrime + rightPrime

  return (-2 * nu * phiPrime) / phi + 4
}

export interface Lesson4DisplayOptions {
  displayDomain: boolean
  displayGrid: boolean
}

export class Lesson4 extends Lesson {
  private defaultPipeline: DefaultPipeline
  private flowPipeline: FlowPipeline

  private flowOptions: FlowOptions
  private displayOptions: Lesson4DisplayOptions

  private timeStepSecs: number = 0

  constructor(
    gl: WebGLRenderingContext,
    flowOptions: FlowOptions,
    displayOptions: Lesson4DisplayOptions
  ) {
    super(gl)

    // Disable depth testing, so that objects are occluded based on drawing order.
    gl.disable(gl.DEPTH_TEST)

    this.defaultPipeline = new DefaultPipeline(gl)
    this.flowPipeline = new FlowPipeline(gl, lesson4Frag, 0.1)

    this.flowOptions = flowOptions
    this.displayOptions = displayOptions
    this.setFlowOptions(this.flowOptions)
  }

  public setFlowOptions(options: Partial<FlowOptions>) {
    this.flowOptions = { ...this.flowOptions, ...options }

    this.timeStepSecs = this.flowOptions.gridSize * this.flowOptions.viscosity

    this.flowPipeline.setOptions(this.flowOptions, (x: number) =>
      uFunc(0, x, this.flowOptions.viscosity)
    )

    this.setDisplayOptions(this.displayOptions)
  }

  public setDisplayOptions(options: Partial<Lesson4DisplayOptions>) {
    this.displayOptions = { ...this.displayOptions, ...options }

    this.defaultPipeline.clear()

    if (this.displayOptions.displayGrid) {
      const gridAxis = new Mesh3(MeshType.LINES)

      const gridLines = this.flowOptions.domainSize / this.flowOptions.gridSize

      for (let i = 1; i < gridLines; i++) {
        const t = i / gridLines
        const x = t - 1 + t
        gridAxis.positions.push([x, -1, 0], [x, 1, 0])
      }
      gridAxis.normals = Array(gridAxis.positions.length).fill([0, 0, 0])
      gridAxis.uvs = Array(gridAxis.positions.length).fill([0, 0, 0])

      this.defaultPipeline.addMesh(gridAxis, [0.2, 0.2, 0.2])
    }

    if (this.displayOptions.displayDomain) {
      const domainAxis = new Mesh3(MeshType.LINES)
      domainAxis.positions.push([-1, 0, 0], [1, 0, 0])
      domainAxis.positions.push([-1, -0.1, 0], [-1, 0.1, 0])

      for (let i = 0; i < this.flowOptions.domainSize; i++) {
        const t = (i + 1) / this.flowOptions.domainSize
        const x = t - 1 + t
        domainAxis.positions.push([x, -0.1, 0], [x, 0.1, 0])
      }
      domainAxis.normals = Array(domainAxis.positions.length).fill([0, 0, 0])
      domainAxis.uvs = Array(domainAxis.positions.length).fill([0, 0, 0])

      this.defaultPipeline.addMesh(domainAxis, [0, 1, 0])
    }
  }

  public resize(width: number, height: number) {
    super.resize(width, height)
  }

  public getTimeStepSecs(): number {
    return this.timeStepSecs
  }

  public fixedStepUpdate(status: UpdateLoopStatus) {
    if (this.timeStepSecs !== status.updateTimeStepSecs) {
      status.updateTimeStepSecs = this.timeStepSecs
    }
    this.flowPipeline.update(status)
  }

  public frameUpdate(_status: UpdateLoopStatus) {
    const gl = this.gl

    gl.viewport(0, 0, this.size[0], this.size[1])
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

    this.defaultPipeline.draw()
    this.flowPipeline.draw()
  }
}
