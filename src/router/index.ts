import { createRouter, createWebHistory } from 'vue-router'
import AboutView from '../views/AboutView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'about',
      component: AboutView
    },
    {
      path: '/test',
      name: 'test',
      // route level code-splitting
      // this generates a separate chunk (Test.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import('../views/TestView.vue')
    },
    {
      path: '/lesson1',
      name: 'lesson1',
      component: () => import('../views/Lesson1View.vue')
    },
    {
      path: '/lesson3',
      name: 'lesson3',
      component: () => import('../views/Lesson3View.vue')
    },
    {
      path: '/lesson4',
      name: 'lesson4',
      component: () => import('../views/Lesson4View.vue')
    }
  ]
})

export default router
