#version 100
precision highp float;
precision highp int;

uniform float nonLinear;
uniform float c; // wave speed
uniform float dx; // grid spacing
uniform float dt; // time step

uniform vec2 flowSize;
uniform sampler2D flowMap;

void main() {
  vec2 uv_0 = fract((gl_FragCoord.xy - vec2(0.0, 0.0)) / flowSize);
  vec2 uv_1 = fract((gl_FragCoord.xy - vec2(1.0, 0.0)) / flowSize);

  float u_0 = texture2D(flowMap, uv_0).x;
  float u_1 = texture2D(flowMap, uv_1).x;

  float speed = (nonLinear * u_0 + (1.0 - nonLinear)) * c;
  float u = u_0 - speed * dt / dx * (u_0 - u_1);

  gl_FragColor = vec4(u, 0.0, 0.0, 0.0);
}
