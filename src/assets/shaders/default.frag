#version 100
precision highp float;
precision highp int;

varying vec3 worldPosition;
varying vec3 worldNormal;
varying vec2 vertexUv;
varying vec3 vertexColor;

uniform int displayMode;
uniform vec3 uniformColor;

void main() {
  vec3 outColor = vec3(1.0);
  vec3 normal = normalize(worldNormal);

  if (displayMode == 0) {
    outColor = worldPosition;

  } else if (displayMode == 1) {
    outColor = normal * 0.5 + 0.5;

  } else if (displayMode == 2) {
    outColor = vec3(vertexUv, 1.0);

  } else if (displayMode == 3) {
    outColor = vertexColor;
  
  } else if (displayMode == 4) {
    outColor = uniformColor;
  }

  gl_FragColor = vec4(outColor, 1.0);
}
