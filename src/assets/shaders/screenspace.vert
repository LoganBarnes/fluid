#version 100
precision highp float;
precision highp int;

attribute vec2 clipPosition;

varying vec2 uv;

void main() {
  uv = clipPosition.xy * 0.5 + 0.5;
  gl_Position = vec4(clipPosition, 0.5, 1.0);
}
