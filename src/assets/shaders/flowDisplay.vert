#version 100
precision highp float;
precision highp int;

attribute vec3 index;

uniform vec2 flowSize;
uniform sampler2D flowMap;
uniform float scaleFactor;

void main() {
  vec2 uv = index.xy / flowSize;
  float flow = texture2D(flowMap, uv).x;
  float x = (index.x + index.z - 0.5) / flowSize.x;
  gl_Position = vec4(x * 2.0 - 1.0, flow * scaleFactor, 0.5, 1.0);
}
