#version 100
precision highp float;
precision highp int;

uniform float dx; // grid spacing
uniform float dt; // time step
uniform float nu; // viscosity

uniform vec2 flowSize;
uniform sampler2D flowMap;

void main() {
  vec2 uv_prev = fract((gl_FragCoord.xy + vec2(-1.0, 0.0)) / flowSize);
  vec2 uv_curr = fract((gl_FragCoord.xy + vec2(+0.0, 0.0)) / flowSize);
  vec2 uv_next = fract((gl_FragCoord.xy + vec2(+1.0, 0.0)) / flowSize);

  float u_prev = texture2D(flowMap, uv_prev).x;
  float u_curr = texture2D(flowMap, uv_curr).x;
  float u_next = texture2D(flowMap, uv_next).x;

  float convection = u_curr * dt / dx * (u_curr - u_prev);
  float diffusion = nu * dt / (dx * dx) * (u_next - 2.0 * u_curr + u_prev);
  float u = u_curr - convection + diffusion;

  gl_FragColor = vec4(u, 0.0, 0.0, 0.0);
}
